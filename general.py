# dimensional.py
#
# These methods handle the various parts of the game taking place in the hilltop
#
# These imports are to be uncommented only for tests
import time
import sys
import re

delay = 0.05

def normal_print():
	global delay
	delay = 0

def clear_space():
	for i in range(0, 100):
		print

def roll_scene(message, audio, start_time):
	pygame.mixer.load(audio)
	pygame.mixer.play(loops=0, start=start_time)
	timed_print(message)

# This method prints out the given text in a slowish format
# This gives the text a "being written" effect
def timed_print(message):
    for char in message:
        sys.stdout.write( '%s' % char)
        sys.stdout.flush()
        time.sleep(delay)
    print ""


# Tests of the methods
# timed_print("Hello World")