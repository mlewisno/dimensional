# dimensional.py
#
# A text based adventure game
# with audio enhancement
# 
# This is the basis of the game. Particular areas of exploration are placed in particular files
#
#
# Import the pygame library
# Pygame will be the basis for all of our audio
import pygame

# Import standard library items needed
import sys
import imp
import os
import re
import time

# Import the general methods module
import general
import menu
import credits

# Import the levels
import the_hilltop
import rest_stop

# Shows the intro text for the game
def intro():
	general.timed_print ("Welcome to the Oberlin College Empathy Experiment")
	general.timed_print ("Your test subject is Subject 3856. You will be living through their memories,")
	general.timed_print ("and more importantly you will be able to explore each particular memory")
	general.timed_print ("more deeply than the subject did.")
	print
	general.timed_print ("You shall begin at one of the subject's more poingnent memories,")
	general.timed_print ("a frozen Illinois Rest Stop on a frigid Januray night...")
	print
	print 
	print "Press Enter to continue"
	tmp = raw_input();


# The actual game code
menu.display_welcome()

in_game = 0

while not in_game:
	result = menu.setup_menu(["1. To Play The Game please type \"Play\"", "2. To Disable SlowType, type \"Faster\"", "3. To View The Credits, please type \"Credits\"", "4. To Quit, please type \"Quit\""])

	# If they said quit, quit
	if result == 4:
		sys.exit(0)

	elif result == 3:
		credits.roll_credits()

	elif result == 2:
		general.normal_print()

	else:
		in_game = 1

general.clear_space()
intro()
rest_stop.just_arrived()



