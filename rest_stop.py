# rest_stop.py
#
# These methods are for the Rest Stop 
# 

import menu
import general

playground_view = 0

def swing_set():
	general.clear_space()
	general.timed_print("You turn around.")
	general.timed_print("The years have actually been quite kind to the ancient wood and rust-speckled metalwork that comprise the play structure before you.")
	general.timed_print("Any and all wear and damage appear to be merely cosmetic.")
	general.timed_print("There are two slides, one long and straight, the frost making star patterns on its smooth surface,")
	general.timed_print("the other spirals around like a corkscrew. Its color is that of the wine it will soon release unto the world.")
	general.timed_print("Between them is a pseudo-building, all metal bars and wooden buttresses.")
	general.timed_print("Its the kind of structure that is perfect for tag.")
	general.timed_print("Its various tunnels and bridges intertwine like an M.C. Escher drawing, allowing for hasty getaways and stratigic lurking.")
	general.timed_print("To the far right, sitting alone and forlorn, the castaway swingset sits, its chains unmoving in the eerily frozen world.")
	result = menu.setup_menu(["1. To get on the straight slide, type \"Straight\"","2. To get on the corkscrew slide, type \"Corkscrew\"", "3. To get on the swings, type \"Swing\"", "4. To turn back around, type \"Behind\""])

	if result == 1 :
		general.timed_print("The metal is too cold to get on the slide.")

	elif result == 2 :
		general.timed_print("You get on the slide.")
		general.timed_print("You go down the slide.")
		general.timed_print("It provides mild amusement.")

	elif result == 3 :
		swing_set()

	elif result == 4 :
		just_arrived()

def rest_stop_second_floor():
	general.timed_print("Welcome to the second floor of the Rest Stop")

def ambulance():
	general.timed_print("Welcome to the ambulance")

def in_the_playground():
	global playground_view
	if not playground_view :
		result = menu.setup_menu(["1. To see what the Subject sees, type \"See\"","2. To hear what is the Subject hears, type \"Listen\"", "3. To feel what the subject feels, type \"Feel\""])
	else :
		result = menu.setup_menu(["1. To see what the Subject sees, type \"See\"","2. To hear what is the Subject hears, type \"Listen\"", "3. To feel what the subject feels, type \"Feel\"", "4. To go to the rest stop, type \"Rest\"", "5. To go to the ambulance, type \"Ambulance\"", "6. To turn around, type \"Behind\""])
	if result == 1:
		general.timed_print("Frozen snowflakes sit suspended in the air, each crystalline structure reflecting an infinite rainbow of color.")
		general.timed_print("They are outlined in alternating halos of blue and red as the lights of an ambulance parked in the parking lot dance.")
		general.timed_print("The ground is covered in a light dusting of snow. The snow is being constantly shifted and thrown into the air in thin white wisps by the frigid wind.")
		general.timed_print("Above, a towering building stands guard like a sphinx risen from the dead. Its western wall juts out like an aristocratic chin.")
		general.timed_print("At the center, where the wall protrudes most, two great windows allow the occupants to see the grandiose Illinois landscape.")
		general.timed_print("Cement stairs lead up to the windows, and on either side a door permits entrance.")
		playground_view = 1

	elif result == 2 :
		general.timed_print("Absolute silence.")

	elif result == 3 :
		general.timed_print("Despite the snow, the air feels oddly warm.")

	return result

def just_arrived():
	general.clear_space()
	result = in_the_playground()
	while result < 4 :
		result = in_the_playground()
		general.clear_space()

	if result == 4 :
		rest_stop_second_floor()

	elif result == 5 :
		ambulance()

	elif result == 6 :
		swing_set()
