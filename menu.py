# menu.py
#
# The methods needed to generate menus.
# The setup_menu method can take in an array of menu options with a specific format
# (See below)
# and then it will return which menu item was selected once a legtitmate option is chosen
import general
import time
import re
import sys

def display_welcome():
	general.clear_space()
	print "Dimensional: by Mischa Lewis-Norelle"

# Builds a menu out of the passed in array of menu items
def build_menu(array):
	# Prints out the array of options
	for entry in array:
		general.timed_print(entry)
	print
	print
	general.timed_print ("Please select which option you'd like.")
	result = raw_input()
	if(result.lower() == "quit"):
		sys.exit(0)
	time.sleep(0.5)

	# Check to see if the input is in the menu
	for index, entry in enumerate(array):
		term = re.findall(r'\"(.+?)\"', entry)
		for stuff in term:
			if result.lower() == stuff.lower():
				index += 1
				return index
	return -1

# Sets up the menu and waits until there is proper input before returning the result
def setup_menu(array):
	choice = build_menu(array)
	while(choice < 1):
		general.clear_space()
		general.timed_print("That was an incorrect answer, please try again")
		choice = build_menu(array)
	return choice
